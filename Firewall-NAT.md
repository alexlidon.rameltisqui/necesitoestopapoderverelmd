# Tallafocs amb IP Tables (II)
https://wiki.archlinux.org/index.php/Iptables_(Espa%C3%B1ol)
https://wiki.archlinux.org/index.php/Simple_stateful_firewall_(Espa%C3%B1ol)

## Tallafocs amb NAT i reenviament de ports (DNAT). Control d’estat de les connexions.

- Configura un equip virtual amb tallafocs IPTables. Aquest tallafocs serà un equip virtual Ubuntu Server amb dues interfícies.
    - La primera amb interfície pont que serà la targeta WAN externa (a la xarxa de l’aula).
    - La segona, amb interfície interna, serà la targeta a la xarxa LAN interna.

![](images/firewall-NAT-743f9f3b.png)

- Determina quines adreces faràs servir a la xarxa interna (proposta, xarxa 192.168.1.0). Recorda activar l’enrutament a l’equip firewall.
 - De forma temporal así
 - ![](images/Selección_001.png)
 - De forma permanente descomentando en **etc/sysctl.conf**
 - ![](images/Selección_002.png)

- Prepara dos equips virtuals, un amb Ubuntu Desktop i un altre amb Ubuntu Server, tots dos amb interfície de xarxa interna. Instal·la un servidor web i un servidor SSH a l’Ubuntu Server intern.
- FLUSH de reglas
- ![](images/Selección_003.png)
- Establecemos politica por defecto
- ![](images/Selección_004.png)


- Es vol que la interfície externa del firewall faci NAT de manera que farem servir només una adreça “pública” (en realitat és una adreça de l'aula).
    - Tots els equips de la xarxa interna han de poder accedir a Internet mitjançant el firewall que enmascararà les seves adreces IP.
    - No s’acceptaran paquets que vinguin de fora i no hi hagi una petició prèvia des dels hosts de la xarxa interna.
     - Aceptamos para que puedan acceder a internet
     - ![](images/Selección_005.png)
     - Ahora hacemos enmascaramiento de la red local
     - ![](images/Selección_006.png)
     - Trancamos los puertos para que no se pueda acceder sino a lo que queremos
     - ![](images/Selección_007.png)
     - Asi queda de momento
     - ![](images/Selección_008.png)
     - Como se ve en la imagen tengo conexión a internet con un equipo de la red interna
     - ![](images/Selección_0012.png)
    - El firewall tindrà instal·lat el servei SSH però al port 2222 i s'hi podrà accedir des de fora.
     - En el firewall ponemos el servidor ssh en el puerto 2222
     - ![](images/Selección_009.png)
     - Añadimos reglas al IPTABLES para que se pueda acceder al puerto desde fuera
     - ![](images/Selección_0010.png)
     - Intentamos acceder desde equipo fisico
     - ![](images/Selección_0011.png)
    - Com a excepció, s’acceptaran els paquets que arribin al firewall al port 2200 i al 80 i 443 que seran reenviats (DNAT) als serveis SSH (al port 22) i WEB (al port 80 i 443), que tenim a l’equip Ubuntu Server intern.
    - Cal guardar als logs cada petició que es faci des de l’equip Desktop.

Proves:
    - Comprova que quan fas un scanneig de ports des de la màquina física al firewall només estan oberts els ports dels serveis actius.
    - Comprova que des del client Ubuntu Desktop i el servidor es pot accedir a Internet (obre un navegador i fes ping al teu equip físic, recordeu que existeixen navegadors en mode consola, com ara el Lynx. També podeu fer servir la comanda wget o curl).
    - Comprova al teu equip físic que les peticions que genera un equip de la xarxa local són vistes com a peticions del firewall, per exemple amb wireshark (amb IP d’origen la del firewall).
    - Comprova que no es pot fer ping ni accedir als equips de la xarxa interna posant l’adreça del servidor de la xarxa local, tot i que haguem posat a la taula de rutes del nostre equip físic la informació per arribar a la xarxa local.

## PFSense
    - Repetiu el procés però fent servir de Firewall un equip amb un Pfsense (no cal crear la resta d’equips, podeu fer-los servir connectant al servidor Pfsense).

![](imgs/firewall-NAT-e7da8496.png)
Reenviament de ports
